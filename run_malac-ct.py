import os
import argparse
from file_name_and_extension_extractor import *
from subprocess import Popen, PIPE, STDOUT

parser = argparse.ArgumentParser(usage="%(prog)s [-i]")
parser.add_argument(
    "-i", "--input", help='file containing a list of paths of the resources to be converted by MaLaC-CT', required=True
)
args = parser.parse_args()

resource_file_path_list = []
with open(args.input, "r") as file:
    file_lines = file.read()
# read content of file only if it is not empty
if file_lines != "":
    resource_file_path_list = file_lines.split("\n")

# eliminate duplicate entries from git_log
resource_file_path_list = list(dict.fromkeys(resource_file_path_list))

print("---------------------------------------------------")
print("Determined resource file paths (duplicates eliminated):")
print("")
print(resource_file_path_list)
print("")
print("---------------------------------------------------")

# tuple specifying the order of file formats converted by malac-ct.
#  - I.e. if for resource "A" the file formats ".x" and ".y" have been changed, only ".y" will be passed on to
#    malac-ct if ".y" comes before ".x"
#  - file formats that do not exist in this tuple will not be passed on to malac-ct
# for MIGRATION phase
resource_type_file_format_priorities = {"CodeSystem":(".2.claml.xml", ".1.svsextelga.xml", ".3.claml.xml", ".1.propcsv.csv", ".2.fsh", ".1.fsh"),
                                        "ValueSet":(".1.svsextelga.xml", ".2.claml.xml", ".3.claml.xml", ".1.propcsv.csv", ".2.fsh", ".1.fsh")}
# for PRODUCTION phase
#file_format_priorities = (".1.propcsv.csv", ".2.fsh", ".1.fsh", ".3.claml.xml", ".2.claml.xml")

changed_resource_file_path_dict, changed_resource_name_set = retrieve_resource_file_path_and_name_set(resource_file_path_list, resource_type_file_format_priorities)

# list of parameters for malac-ct
parameter_list = ["python", "./malac-ct/malac_ct.py",
        "-langArg", "de-AT",
        "-j2xServer", os.environ['FHIRServer'],
        "-j2xUser", os.environ['FHIRServerUser'],
        "-j2xPw", os.environ['FHIRServerUserPw'],
        "-url", os.environ['TERMGIT_CANONICAL'] + "/",
        "-appndProcTermTo", "./terminologies/terminologiesMetadata.csv",
        "-appndProcTermTo", "./processedTerminologies.csv"]

# if environment variable is set accordingly, add the parameter to the parameter list
# in order to have all artifacts uploaded to the FHIR server
if 'UPLOAD_ARTIFACTS' in os.environ:
    if os.environ['UPLOAD_ARTIFACTS'] == 'true':
        parameter_list.append('-up')

# append parameter for input file
parameter_list.append('-i')

for resource_type in changed_resource_file_path_dict.keys():
    print('malac-ct is now processing following resource type: ' + resource_type)
    for resource_file_path in changed_resource_file_path_dict[resource_type]:
        print('malac-ct subprocess for: ' + resource_file_path)
        # append path of resource file
        parameter_list.append("./" + resource_file_path)
        Popen(parameter_list).communicate()
        # remove path of resource file
        parameter_list.pop()
