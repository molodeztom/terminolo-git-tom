> **For the english version please click [here](faq_en.html).**

Alle Informationen zum neuen Terminologieserver und zur Verwendung von Terminologien sind zweisprachig unter [Dokumentation und Support (de)](documentation_and_support_de.html) bzw. [Documentation and Support (en)](documentation_and_support_en.html) abrufbar. Die Dokumentation wird laufend ergänzt und verbessert.

Die wichtigsten Fragen und Antworten zum Terminologieserver sind hier zusammengefasst.

### Wo finde ich Codelisten und Value Sets, die ich bisher verwendet habe?
Über [Table of Contents](toc.html) bzw. [Artifact Summary](artifacts.html) können die Terminologien abgerufen werden. Zur einfacheren Lesbarkeit wurden lediglich die Postfixes (z.B. "_CS" oder "_VS") entfernt. Sobald man auf eine Terminologie klickt, werden in der Summary die Details, wie zum Beispiel ob es sich um ein Codesystem oder Value Set handelt, angezeigt.

Table of Contents listet alle Terminologien alphabetisch. Unter der Artifact Summary werden alle Terminologien nach Codesystemen bzw. Value Sets gruppiert.

Eine erweiterte Suche wird noch implementiert.

### Welche Downloadformate gibt es? Wie entsprechen die neuen Formate jenen des alten Terminologieservers?

Aktuell werden folgende Downloadformate unter dem Reiter `Download` der jeweiligen Terminologie angeboten:

| *neuer* Terminologieserver | *alter* Terminologieserver | Hinweis |
| ----------- | ----------- | ----------- |
| FHIR R4 xml `.4.fhir.xml` | keine Entsprechung       | |
| FHIR R4 json `.4.fhir.json`   | keine Entsprechung        | |
| fsh v1 `.1.fsh`   | keine Entsprechung        | |
| fsh v2 `.2.fsh`   | keine Entsprechung        | |
| ClaML v2 `.2.claml.xml`   | ClaML        | |
| ClaML v3 `.3.claml.xml`   | keine Entsprechung        | |
| propCSV v1 `.1.propcsv.csv`   | keine Entsprechung        | |
| SVSextELGA v1 `.1.svsextelga.xml`   | SVS        | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |
| outdatedCSV v1 `.1.outdatedcsv.csv`   | CSV        | **Achtung! DEPRECATED!**<br/>Dieses Format kann technisch gesehen nicht alle Informationen enthalten,<br/>die in den anderen Formaten verfügbar sind.<br/>Dieses Format ist nur für Legacy-Zwecke verfügbar. |

Eine detailliertere Beschreibung der einzelnen Downloadformate kann unter [Downloadformate](documentation_and_support_de.html#downloadformate) abgerufen werden.

### Wie erkenne ich, welche Terminologie die aktuelle ist?

Die Terminologien, die über [Table of Contents](toc.html) bzw. [Artifact Summary](artifacts.html) angezeigt und aufgerufen werden können, stellen immer die aktuellsten Versionen dar.

Table of Contents listet alle Terminologien alphabetisch. Unter der Artifact Summary werden alle Terminologien nach Codesystemen bzw. Value Sets gruppiert.

### Wo finde ich alte Versionen?

Im neuen Terminologieserver können für jede Terminologie unter dem Reiter `Previous Versions` alle publizierten Versionen abgerufen werden. Bei der Darstellung von alten Versionen wird jeweils ein Warnhinweis angezeigt, dass es sich um eine alte Version handelt. Dieser Hinweis beinhaltet auch einen Link auf die aktuelle Terminologieversion.

Für den Parallelbetrieb wurden zunächst nur die aktuellsten Versionen der Terminologien auf den neuen Terminologieserver migriert. Alte Versionen sind weiterhin unter [https://termpub.gesundheit.gv.at/](https://termpub.gesundheit.gv.at/) abrufbar.

Der alte Terminologieserver wird bis Oktober 2022 betrieben.

An einer Lösung, wie alte Terminologieversionen am neuen Terminologieserver bereitgestellt werden können, wird derzeit noch gearbeitet.

### Wie kann ich von Updates benachrichtigt werden?

Siehe [Automatischer Import von Katalogen](documentation_and_support_de.html#automatischer-import-von-katalogen) bzw. [Manuelle Aktualisierung von Terminologien](documentation_and_support_de.html#manuelle-aktualisierung-von-terminologien)

### Wie kann man Änderungen vorschlagen oder Fehler melden?

Siehe [Support und Helpdesk](documentation_and_support_de.html#support-und-helpdesk)
