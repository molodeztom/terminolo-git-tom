> **For the english version please click [here](index_en.html).**

### Willkommen auf dem neuen österreichischen e-Health Terminologiebrowser!

Der Terminologieserver bietet den Stakeholdern des österreichischen Gesundheitswesens die Möglichkeit Terminologien über das Internet bereit zu stellen und ermöglicht Terminologie-Recherche sowie Terminologie-Download in verschiedenen Formaten. Ziel ist es, dass die IT-Systeme, die untereinander in einer Datenaustausch-Beziehung stehen, dieselben Terminologien zur selben Zeit verwenden können.  Codesysteme, Value Sets und Concept-Maps werden gemeinsam mit den dazugehörigen historischen Versionen in standardisierten Formaten und einheitlichen Metadaten leicht zugänglich bereitgestellt.
Eine umfassende Dokumentation ist **[hier zu finden](documentation_and_support_de.html)**. Die Beantwortung der bisher wichtigsten Fragen werden unter den **[FAQs](faq_de.html)** zusammengefasst.


Technologisch basiert der österreichische e-Health Terminologiebrowser auf TerminoloGit. Im Sinne des Open-Source-Gedankens würden sich die Entwickler sehr über die Nutzung von TerminoloGit in anderen Projekten, Feedback und noch mehr über eine aktive Beteiligung freuen.
Verbesserungsvorschläge oder Fehlermeldungen, die bei der Anwendung des Systems erkannt wurden, können über das GitLab-Ticketsystem unter [https://gitlab.com/elga-gmbh/termgit/-/issues/new](https://gitlab.com/elga-gmbh/termgit/-/issues/new) eingemeldet werden.
Für etwaige Anfragen zu den Terminologien steht das ELGA-Jira Kundenportal ([https://jira-neu.elga.gv.at/servicedesk/customer/portal/3](https://jira-neu.elga.gv.at/servicedesk/customer/portal/3)) zur Verfügung. Hier ist eine kurze kostenlose Selbstregistrierung erforderlich.

Außerdem besteht weiterhin die Möglichkeit Fragen über das E-Mailpostfach [cda@elga.gv.at](mailto:cda@elga.gv.at) einzubringen.