import os
import re
import csv
from subprocess import Popen, PIPE, STDOUT
from pathlib import Path
from sys import float_repr_style
from shutil import copyfile
from file_name_and_extension_extractor import *

def create_tag(tag_name):
    if tag_name:
        print('remove and add tag for ' + tag_name)
        # remove tag
        os.system('git tag -d ' + tag_name)
        # add tag
        os.system('git tag ' + tag_name)

processed_resource_dict = {}
with open('processedTerminologies.csv', 'r', newline='') as csvfile:
    csv_reader = csv.DictReader(csvfile, delimiter=',')
    for one_row in csv_reader:
        if one_row['name']:
            processed_resource_dict[one_row['name']] = (one_row['oid'], one_row['version'])

# read Git-hash of last successful pipeline run
with open("lastSuccessfulPipeline.txt", "r") as file:
    pipeline_sha = file.read()

print("commit sha for last successful pipeline: " + pipeline_sha)

zuppl_file_path_basename_dict = dict()
# if resources have been changed also check for zuppl-files that might have been changed/created
if len(processed_resource_dict) > 0:
    # change into root directory (= terminologit repository) in order to be able
    # - to determine which zuppl-files might have been changed/created
    os.chdir('../..')

    # get all changes for the whole from the last successful pipeline run till now within "terminologies" directory

    git_log = []
    if (pipeline_sha != ""):
        # get all changes for the whole from the last successful pipeline run till now within "terminologies" directory
        process = Popen(["git","log",pipeline_sha + "..HEAD","--name-only","--pretty=","terminologies/"], stdout=PIPE, stderr=STDOUT, bufsize=1)
        with process.stdout:
            # automatically decode as utf-8 and remove \n
            git_log = [x.decode('utf8')[:-1] for x in process.stdout.readlines()]
    else:
        # get all changes for the whole git log within "terminologies" directory
        process = Popen(["git","log","--name-only","--pretty=","terminologies/"], stdout=PIPE, stderr=STDOUT, bufsize=1)
        with process.stdout:
            # automatically decode as utf-8 and remove \n
            git_log = [x.decode('utf8')[:-1] for x in process.stdout.readlines()]

    # eliminate duplicate entries from git_log
    git_log = list(dict.fromkeys(git_log))

    for resource_file_path in git_log:
        # consider only those files that
        # - contain "zuppl" in their file path AND
        # - still exist, thus automatically ignoring files that have been deleted
        if "zuppl" in resource_file_path and os.path.exists(resource_file_path):
            # determine resource name and its file format
            resource_name, file_format = file_path_file_name_and_extension_extractor(resource_file_path)
            # add the resource name to the list of changed resources
            processed_resource_dict[resource_name] = ('', '')
            zuppl_file_path_basename_dict[resource_name] = os.path.dirname(resource_file_path)

    # change into directory of termgit-html-project/output
    os.chdir('termgit-html-project/output')

print("-------- processed_resource_dict --------")
print(processed_resource_dict)

# retrieve the date of the last commit
p2 = Popen(["git","show","-s","--format=%ci","HEAD"], stdout=PIPE, stderr=STDOUT, bufsize=1)
with p2.stdout:
    prevCommitDatetime = [x.decode('utf8') for x in p2.stdout.readlines()][0]
# date will be converted from "yyyy-MM-dd hh:mm:ss +zzzz\n" to "yyyyMMdd-hhmmss-"
prevCommitFileDatetime = prevCommitDatetime[:-6].replace("-","").replace(":","").replace(" ","-")
os.system('echo previous commit datetime: '+prevCommitFileDatetime)

git_add_new_previous_versions_files=[]

# make sure includes directory exists
Path("../../input/includes/").mkdir(parents=True, exist_ok=True)

# for every processed resource
for resourceName in processed_resource_dict.keys():
    os.system('echo next resource is: ' + resourceName)
    # file path to the list of previous versions
    previousVersionsFile = "../../input/includes/"+resourceName+"-previous-versions.xhtml"
    # create list of previous versions if it does not exist
    if not os.path.exists(previousVersionsFile):
        with open(previousVersionsFile,'w',encoding='utf-8') as fileName:
            fileName.writelines('<table class="grid">\r\n<tr><td><b>Publication Date</b></td><td><b>Current Version vs. Outdated Version</b></td></tr>\r\n</table>')
        # store file path to new previous version file
        git_add_new_previous_versions_files.append(previousVersionsFile[4:])
    else:
        with open(previousVersionsFile,'r',encoding='utf-8') as fileName:
            lines = fileName.readlines()
            # remove last line containing end of table "</table>"
            lines = lines[:-1]
            # add line with link to last published version and possibility to create diff
            lines.append('<tr>'+
            '<td><a href="'+prevCommitFileDatetime+resourceName+".html"+'">'+prevCommitDatetime[:-7]+'</a></td>'+
            '<td><a onclick="createDiff(\''+resourceName+'.html\', \''+prevCommitFileDatetime+resourceName+'.html\')" href="javascript:void(0);">Diff</a></td>'+
            '</tr>\r\n')
            # terminate table
            lines.append('</table>')
        # save it under ../../input/includes
        with open(previousVersionsFile,'w',encoding='utf-8') as fileName:
            fileName.writelines(lines)

        # change the links in all unversioned htmls and write the banner into the html and rename all the files without a date
        for fileName in Path('.').rglob('*'):
            if not re.match("\d{8}-\d{6}-.+",fileName.name) and not fileName.name.endswith(".previous-versions.html") and fileName.name.startswith(resourceName + "."):
                if fileName.name.endswith(".html"):
                    with open(fileName,'r',encoding="utf-8") as file:
                        lines = file.readlines()
                        lineIterator = iter(lines)
                        for line in lineIterator:
                            # - change lines where 'href="resourceName' exists but
                            # - do not change link to .previous-versions.html
                            if not ".previous-versions.html" in line and 'href="'+resourceName in line:
                                lines[lines.index(line)] = line.replace(resourceName,prevCommitFileDatetime+resourceName)
                            # add banner
                            if '<ul class="nav nav-tabs">' in line:
                                lines.insert(lines.index(line),'<div role="alert" class="alert alert-danger">\r\n<p id="publish-box">This is an outdated version that is no longer valid! You can access the <a href='+resourceName+'.html>current version here</a>.</p>\r\n</div>')
                                # directly jump to next line - otherwise, the info about earlier versions will be inserted endlessly
                                next(lineIterator)

                    # save file
                    with open(fileName,'w',encoding='utf-8') as file:
                        file.writelines(lines)

                # rename file
                os.rename(fileName.name,prevCommitFileDatetime+fileName.name)

# move all versioned files to the old directory
# move all *zuppl* files to the old directory - Zuppl files are legacy and are no longer supported. However, they will be preserved in order to have functional old versions (the reference Zuppl terminologies)
for fileName in Path('.').rglob('*'):
    if re.match("\d{8}-\d{6}-.+",fileName.name) or re.match(".*zuppl.*", fileName.name):
        copyfile(fileName.name, "../../old/"+fileName.name)

# change into root directory (= terminologit repository) in order to be able
# - to determine the full hash of the last commit
# - to (re-)set the tags of the changed terminologies accordingly (if on default branch)
os.chdir('../..')

# get the last commit's full hash, from https://www.systutorials.com/how-to-get-the-latest-commits-hash-in-git/
p3 = Popen(["git","log","-n1","--format=format:\"%H\""], stdout=PIPE, stderr=STDOUT, bufsize=1)
with p3.stdout:
    gitFullHash = p3.stdout.readline().decode('utf8')

os.system('echo full commit hash: ' + gitFullHash)

downloadBase = "https://gitlab.com/"+os.environ['CI_PROJECT_PATH']+"/-/raw/"+gitFullHash.strip('"')

# add for all changed files a new download html
for resourceName in processed_resource_dict.keys():
    os.system('echo downloads for the following ressource are created: ' + resourceName)
    with open('./input/includes/'+resourceName+'-download.xhtml', 'w') as file:
        downloadResourceBase = downloadBase + "/terminologies/" + resourceName + "/" + resourceName
        if "zuppl" in resourceName:
            downloadResourceBase = downloadBase + "/" + zuppl_file_path_basename_dict[resourceName] + "/" + resourceName

        os.system('echo downloadResourceBase: ' + downloadResourceBase)
        file.writelines('<table class="grid">\r\n')
        if "zuppl" in resourceName:
            file.writelines('<tr><td><a href="' + downloadResourceBase + '.4.fhir.json?inline=false">download FHIR R4 json</a></td><td><a target="_blank" rel="noopener noreferrer" href="' + downloadResourceBase + '.4.fhir.json">raw FHIR R4 json (from GitLab)</a></td></tr>\r\n')
        else:
            for implementedFormat, implementedFormatMetainfo in implemented_file_formats.items():
                file.writelines('<tr><td><a href="' + downloadResourceBase + implementedFormat + '?inline=false">download ' + implementedFormatMetainfo[0] + '</a></td><td><a target="_blank" rel="noopener noreferrer" href="' + downloadResourceBase + implementedFormat + '">raw ' + implementedFormatMetainfo[0] + ' (from GitLab)</a></td><td>' + implementedFormatMetainfo[1] + '</td></tr>\r\n')
        file.writelines('</table>')

# job will only run if commit branch is on default branch and if there are resources that have been changed
if os.environ['CI_COMMIT_BRANCH'] == os.environ['CI_DEFAULT_BRANCH'] and len(processed_resource_dict) > 0:
    # add/update tags for every changed resource
    for changedResource in processed_resource_dict.keys():
        # add tags for non-zuppl terminologies only
        if "zuppl" not in changedResource:
            # create Git tag for resource name
            create_tag(changedResource)
            # create Git tag for resource's OID
            create_tag(processed_resource_dict[changedResource][0])

    # push the tags
    print('pushing tags')
    os.system('git push -f --tags "https://'+os.environ['GITLAB_USER_LOGIN']+':'+os.environ['GITLAB_CI_TOKEN']+'@gitlab.com/'+os.environ['CI_PROJECT_PATH']+'.git"')

# write the file paths of the new created previous version files for them to be commited later
with open("new_previous_versions_files.txt", "w") as file:
    file_lines = " ".join(git_add_new_previous_versions_files)
    print('new previous versions files: ' + file_lines)
    file.write(file_lines)
